package myself.ifu.test;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import myself.ifu.DevStreamParser;

public class DevStreamParserTest extends TestCase {
	
	public static String CASE1 = "Inter-|   Receive                                                |  Transmit\n" +
								 "face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed\n" +
    							 "lo: 1437107    9845    0    0    0     0          0         0  1437107    9845    0    0    0     0       0          0\n" +
    							 "eth0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0";

	public void testReadHeader() throws IOException {
		StringReader reader = new StringReader(CASE1);
		DevStreamParser parser = new DevStreamParser(reader);
		List<String> header = parser.readHeader();
		assertNotNull(header);
		assertEquals(header.size(), 17);
		assertEquals(header.get(0), DevStreamParser.INTERFACE);
		assertEquals(header.get(1), DevStreamParser.RX_BYTES);
		assertEquals(header.get(2), DevStreamParser.RX_PACKETS);
		assertEquals(header.get(3), DevStreamParser.RX_ERRORS);
		assertEquals(header.get(4), DevStreamParser.RX_DROPS);
		assertEquals(header.get(5), DevStreamParser.RX_FIFO);
		assertEquals(header.get(6), DevStreamParser.RX_FRAMES);
		assertEquals(header.get(7), DevStreamParser.RX_COMPRESSED);
		assertEquals(header.get(8), DevStreamParser.RX_MULTICAST);
		assertEquals(header.get(9), DevStreamParser.TX_BYTES);
		assertEquals(header.get(10), DevStreamParser.TX_PACKETS);
		assertEquals(header.get(11), DevStreamParser.TX_ERRORS);
		assertEquals(header.get(12), DevStreamParser.TX_DROPS);
		assertEquals(header.get(13), DevStreamParser.TX_FIFO);
		assertEquals(header.get(14), DevStreamParser.TX_COLLISIONS);
		assertEquals(header.get(15), DevStreamParser.TX_CARRIER);
		assertEquals(header.get(16), DevStreamParser.TX_COMPRESSED);
	}
	
	public void testReadDevice1() throws IOException {
		StringReader reader = new StringReader(CASE1);
		DevStreamParser parser = new DevStreamParser(reader);
		parser.readHeader();
		Map<String, String> dev = parser.readDevice();
		assertNotNull(dev);
		assertEquals(dev.size(), 17);
		assertEquals(dev.get(DevStreamParser.INTERFACE), "lo:");
		assertEquals(dev.get(DevStreamParser.RX_BYTES), "1437107");
		assertEquals(dev.get(DevStreamParser.RX_PACKETS), "9845");
		assertEquals(dev.get(DevStreamParser.RX_ERRORS), "0");
		assertEquals(dev.get(DevStreamParser.RX_DROPS), "0");
		assertEquals(dev.get(DevStreamParser.RX_FIFO), "0");
		assertEquals(dev.get(DevStreamParser.RX_FRAMES), "0");
		assertEquals(dev.get(DevStreamParser.RX_COMPRESSED), "0");
		assertEquals(dev.get(DevStreamParser.RX_MULTICAST), "0");
		assertEquals(dev.get(DevStreamParser.TX_BYTES), "1437107");
		assertEquals(dev.get(DevStreamParser.TX_PACKETS), "9845");
		assertEquals(dev.get(DevStreamParser.TX_ERRORS), "0");
		assertEquals(dev.get(DevStreamParser.TX_DROPS), "0");
		assertEquals(dev.get(DevStreamParser.TX_FIFO), "0");
		assertEquals(dev.get(DevStreamParser.TX_COLLISIONS), "0");
		assertEquals(dev.get(DevStreamParser.TX_CARRIER), "0");
		assertEquals(dev.get(DevStreamParser.TX_COMPRESSED), "0");
	}
	
	public void testReadDevice2() throws IOException {
		StringReader reader = new StringReader(CASE1);
		DevStreamParser parser = new DevStreamParser(reader);
		parser.readHeader();
		parser.readDevice();
		Map<String, String> dev = parser.readDevice();
		assertNotNull(dev);
		assertEquals(dev.size(), 17);
		assertEquals(dev.get(DevStreamParser.INTERFACE), "eth0:");
		assertEquals(dev.get(DevStreamParser.RX_BYTES), "0");
		assertEquals(dev.get(DevStreamParser.RX_PACKETS), "0");
		assertEquals(dev.get(DevStreamParser.RX_ERRORS), "0");
		assertEquals(dev.get(DevStreamParser.RX_DROPS), "0");
		assertEquals(dev.get(DevStreamParser.RX_FIFO), "0");
		assertEquals(dev.get(DevStreamParser.RX_FRAMES), "0");
		assertEquals(dev.get(DevStreamParser.RX_COMPRESSED), "0");
		assertEquals(dev.get(DevStreamParser.RX_MULTICAST), "0");
		assertEquals(dev.get(DevStreamParser.TX_BYTES), "0");
		assertEquals(dev.get(DevStreamParser.TX_PACKETS), "0");
		assertEquals(dev.get(DevStreamParser.TX_ERRORS), "0");
		assertEquals(dev.get(DevStreamParser.TX_DROPS), "0");
		assertEquals(dev.get(DevStreamParser.TX_FIFO), "0");
		assertEquals(dev.get(DevStreamParser.TX_COLLISIONS), "0");
		assertEquals(dev.get(DevStreamParser.TX_CARRIER), "0");
		assertEquals(dev.get(DevStreamParser.TX_COMPRESSED), "0");
	}

}
