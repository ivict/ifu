package myself.ifu;

import static myself.ifu.R.id.*;
import android.view.View;
import android.widget.TextView;

public class LinkInfoCtrl {

	private final View view;
	
	public LinkInfoCtrl(View view) {
		this.view = view;
	}

	private void showText(String textValue, int resourceId) {
		TextView textView = (TextView) view.findViewById(resourceId);
		textView.setText(textValue);
	}

	public void showLinkEncap(String linkEncap) {
		showText(linkEncap, link_encap_value);
	}
	
	public void showInetInfo(String inetAddr, String inetMask) {
		showText(inetAddr, inet_addr_value);
		showText(inetMask, inet_mask_value);
		showRow(inet_info);
	}

	private void showRow(int viewId) {
		View row = (View) view.findViewById(viewId);
		row.setVisibility(View.VISIBLE);
	}

	public void showInet6Info(String inet6Addr, String inet6Scope) {
		showText(inet6Addr, inet6_addr_value);
		showText(inet6Scope, inet6_scope_value);
		showRow(inet6_info);
	}
	
	public void showStatistics(String mtu, String metric, 
			String rxPackets, String rxErrors, String rxDropped, String rxOverruns, String rxFrame,
			String txPackets, String txErrors, String txDropped, String txOverruns, String txCarrier,
			String collisions, String txqueuelen, String rxBytes, String txBytes) {
		showText(mtu, mtu_value);
		showText(metric, metric_value);
		showText(rxPackets, rx_packets_value);
		showText(rxErrors, rx_errors_value);
		showText(rxDropped, rx_dropped_value);
		showText(rxOverruns, rx_overruns_value);
		showText(rxFrame, rx_frame_value);
		showText(txPackets, tx_packets_value);
		showText(txErrors, tx_errors_value);
		showText(txDropped, tx_dropped_value);
		showText(txOverruns, tx_overruns_value);
		showText(txCarrier, tx_carrier_value);
		showText(collisions, collisions_value);
		showText(txqueuelen, txqueuelen_value);
		showText(rxBytes, rx_bytes_value);
		showText(txBytes, tx_bytes_value);
	}
	
	public void showHardwareInfo(String interrupt, String baseAddress) {
		showText(interrupt, interrupt_value);
		showText(baseAddress, base_address_value);
		showRow(hardware_info);
	}

}
