package myself.ifu;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.app.Activity;

import static myself.ifu.Invariants.*;

public class MainActivity extends Activity {
	
	public static final String TAG = MainActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		showIfaces();
	}

	private void showIfaces() {
		TableLayout main = (TableLayout) findViewById(R.id.main);
		try {
			FileReader reader = new FileReader("/proc/net/dev");
			DevStreamParser parser = new DevStreamParser(reader);
			parser.readHeader();
			Map<String, String> iface;
			while ( !(iface = parser.readDevice()).isEmpty() ) {
				TableRow row = new TableRow(main.getContext());
				View deviceComponent = View.inflate(row.getContext(), R.layout.device_component, row);
				main.addView(row);

				LinkInfoCtrl linkInfoCtrl = new LinkInfoCtrl(deviceComponent);
				linkInfoCtrl.showLinkEncap(iface.get(DevStreamParser.INTERFACE));
			}
			
		} catch ( IOException e ) {
			Log.e(TAG, ERR_IO_EXCEPTION, e);
		}
	}

	

}
