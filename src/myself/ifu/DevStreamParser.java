package myself.ifu;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import static myself.ifu.Invariants.*;

/**
 * Parser of file /proc/net/dev 
 */
public class DevStreamParser {
	
	public static final String TAG = DevStreamParser.class.getName();
	
	public final static String INTERFACE = "Interface";

	public static final String RX_BYTES = "rxBytes";

	public static final String RX_PACKETS = "rxPackets";

	public static final String RX_ERRORS = "rxErrs";
	
	public static final String RX_DROPS = "rxDrop";

	public static final String RX_FIFO = "rxFifo";

	public static final String RX_FRAMES = "rxFrame";

	public static final String RX_COMPRESSED = "rxCompressed";

	public static final String RX_MULTICAST = "rxMulticast";

	public static final String TX_BYTES = "txBytes";

	public static final String TX_PACKETS = "txPackets";

	public static final String TX_ERRORS = "txErrs";

	public static final String TX_DROPS = "txDrop";

	public static final String TX_FIFO = "txFifo";

	public static final String TX_COLLISIONS = "txColls";

	public static final String TX_CARRIER = "txCarrier";

	public static final String TX_COMPRESSED = "txCompressed";
	
	private final LineNumberReader reader;
	
	private List<String> header;
	
	public DevStreamParser(Reader reader) {
		this.reader = new LineNumberReader(reader);
	}
	
	public List<String> readHeader() throws IOException {
		reader.readLine();
		String header2 = reader.readLine();
		String[] groups = header2.split("\\|");
		if ( groups.length < 3 ) {
			Log.w(TAG, WARN_WRONG_HEADER);
			return Collections.emptyList();
		}
		List<String> cols = new ArrayList<String>();
		cols.add(INTERFACE);
		String[] rxCols = groups[1].split("\\s+");
		if ( rxCols.length < 8 ) {
			Log.w(TAG, WARN_WRONG_HEADER);
			return Collections.emptyList();
		}
		for ( String rxCol : rxCols ) {
			cols.add(fmt("rx", rxCol));
		}
		String[] txCols = groups[2].split("\\s+");
		if ( txCols.length < 8 ) {
			Log.w(TAG, WARN_WRONG_HEADER);
			return Collections.emptyList();
		}
		for ( String txCol : txCols ) {
			cols.add(fmt("tx", txCol));
		}
		//Update local cache
		header = new ArrayList<String>(cols);
		return cols;
	}

	private String fmt(String prefix, String col) {
		if ( col == null ) {
			return null;
		}
		if ( col.isEmpty() ) {
			return prefix;
		}
		char firstChar = Character.toUpperCase(col.charAt(0));
		if ( col.length() == 1 ) {
			return prefix + firstChar;
		}
		return prefix + firstChar + col.substring(1);
	}
	
	public Map<String, String> readDevice() throws IOException {
		if ( header == null ) {
			Log.i(TAG, INFO_TRY_TO_PARSE_HEADER);
			return Collections.emptyMap();
		}
		String readedLine = reader.readLine();
		if ( readedLine == null ) {
			return Collections.emptyMap();
		}
		String line = readedLine.trim();
		String[] fields = line.split("\\ +");
		int headerSize = header.size();
		if ( fields.length < headerSize ) {
			Log.w(TAG, WARN_WRONG_LINE);
			return Collections.emptyMap();
		}
		Map<String, String> devStat = new HashMap<String, String>();
		for ( int i = 0; i < headerSize; i++ ) {
			String fieldValue = fields[i];
			String fieldName = header.get(i);
			devStat.put(fieldName, fieldValue);
		}
		return devStat;
	}
	
}
