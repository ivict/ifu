package myself.ifu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TableLayout;

public class DeviceComponent extends TableLayout {
	
	public DeviceComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DeviceComponent(Context context) {
		super(context);
	}

}
