package myself.ifu;

public class Invariants {

	private Invariants () { }

	public static final String WARN_WRONG_HEADER = "Wrong header!";
	public static final String WARN_WRONG_LINE = "Wrong line format!";
	public static final String INFO_TRY_TO_PARSE_HEADER = "Try to parse header before, parsing lines.";
	public static final String ERR_IO_EXCEPTION = "Error input/output.";
	
}
